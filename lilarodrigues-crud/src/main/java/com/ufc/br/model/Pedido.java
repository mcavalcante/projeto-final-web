package com.ufc.br.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import com.ufc.br.model.*;


@Entity
public class Pedido {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id; 
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable( 
	        name = "pedidos_bolos", 
	        joinColumns = @JoinColumn(
	          name = "pedido_id", referencedColumnName = "id"), 
	        inverseJoinColumns = @JoinColumn(
	          name = "bolo_id", referencedColumnName = "id")) 
	private List<Bolo> bolosPedidos;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	

	public List<Bolo> getBolosPedidos() {
		return bolosPedidos;
	}

	public void setBolosPedidos(List<Bolo> bolosPedidos) {
		this.bolosPedidos = bolosPedidos;
	}
	
}
