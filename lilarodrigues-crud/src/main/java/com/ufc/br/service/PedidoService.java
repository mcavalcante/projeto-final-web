package com.ufc.br.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.ufc.br.model.Pedido;
import com.ufc.br.model.Usuario;
import com.ufc.br.repository.pedidoDataBase;

public class PedidoService {
	@Autowired
	public pedidoDataBase pedidoData; 
	
	
	public void cadastrar(Pedido pedido) {
		pedidoData.save(pedido);
	}
	
	public List<Pedido> listaTodos(){
		return pedidoData.findAll();
	}
	
	public void excluir(Long id) {
		pedidoData.deleteById(id);
	}
	
	public Pedido buscarPorId(Long id) {
		return pedidoData.getOne(id);
	}


}
